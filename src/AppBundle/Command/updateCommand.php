<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Service\updateDistrict;

class updateCommand extends Command
{
    private $updateDistrict;

    public function __construct(updateDistrict $updateDistrict)
    {
        $this->updateDistrict = $updateDistrict;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        // the name of the command (the part after "bin/console")
        ->setName('app:update-districts')

        // the short description shown while running "php bin/console list"
        ->setDescription('Fetching district updates.')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command allows you to update district list...')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fetchedRecords = $this->updateDistrict->fetchDistrictsData();
        $output->writeln('Fetched ' . $fetchedRecords . ' records.');
        $output->writeln('End.');
    }
}
