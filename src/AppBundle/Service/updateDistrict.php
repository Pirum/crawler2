<?php

namespace AppBundle\Service;

use Symfony\Component\DomCrawler\Crawler;
use AppBundle\Entity\District;
use Doctrine\ORM\EntityManager;

class updateDistrict
{
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function fetchDistrictsData()
    {
        // City: Gdansk
        $i = 1;
        $isDistrict = true;
        do {
            $url = 'http://www.gdansk.pl/subpages/dzielnice/';
            $url .= '[dzielnice]/html/dzielnice_mapa_alert.php';
            $url .= '?id=' . $i;
            $text = $this->getGdansk($url);

            if (array_key_exists(1, $text[0])) {
                // $em = $this->getDoctrine()->getManager();
                $entity = new District();

                $entity->setCity('Gdańsk');
                $entity->setName(trim($text[0][1]));

                preg_match('!\d+,\d+!', $text[0][2], $area); // extract only number from string
                $entity->setArea((float) str_replace(',', '.', $area[0]));

                preg_match('!\d+!', $text[0][3], $population); // expects only integers
                $entity->setPopulation((int) $population[0]);

                $this->em->persist($entity);
                $this->em->flush();

                ++$i;
            } else {
                $isDistrict = false;
            }
        } while ($isDistrict);

        // City: Krakow
        $j = 1;
        $isDistrict = true;
        do {
            $url = 'http://appimeri.um.krakow.pl/app-pub-dzl/pages/DzlViewGlw.jsf';
            $url .= '?id=' . $j;
            $text = $this->getKrakow($url);

            if (!$text) {
                $isDistrict = false;
                break;
            }

            if (array_key_exists(0, $text[0])) {
                // $em = $this->getDoctrine()->getManager();
                $entity = new District();

                $entity->setCity('Kraków');
                $entity->setName(trim($text[0][0]));

                preg_match('!\d+,\d+!', $text[1][2], $area); // extract only number from string
                $entity->setArea((float) str_replace(',', '.', $area[0]));

                preg_match('!\d+!', $text[1][4], $population); // expects only integers
                $entity->setPopulation((int) $population[0]);

                $this->em->persist($entity);
                $this->em->flush();

                ++$j;
            } else {
                $isDistrict = false;
            }
        } while ($isDistrict);

        // return sum of fetched records
        return $i+$j;
    }

    public function getGdansk($url)
    {
        $html = file_get_contents($url);
        $crawler = new Crawler($html);
        $nodeValues = array();
        $nodeValues[] = $crawler->filter('div')->each(function (Crawler $node, $i) {
            return $node->text();
        });

        return $nodeValues;
    }

    public function getKrakow($url)
    {
        if (!$html = @file_get_contents($url)) {
            return false;
        }

        $crawler = new Crawler($html);
        $nodeValues = array();
        $nodeValues[] = $crawler->filter('h3')->each(function (Crawler $node, $i) {
            // the name of district
            return $node->text();
        });

        $nodeValues[] = $crawler->filter('td')->each(function (Crawler $node, $i) {
            // other district details
            return $node->text();
        });

        return $nodeValues;
    }
}
