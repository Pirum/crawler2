<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\updateDistrict;
use AppBundle\Entity\District;

class DefaultController extends Controller
{
    /**
     * @Route("/update-districts", name="updateDistricts")
     */
    public function indexAction(updateDistrict $updateDistrict)
    {
        $fetchedRecords = $updateDistrict->fetchDistrictsData();
        // $this->addFlash('success', $message);

        return $this->render('default/crawlerResult.html.twig', array(
            'fetchedRecords' => $fetchedRecords
        ));
    }

    /**
     * @Route("/list", name="list_all")
     */
    public function list()
    {
        $repository = $this->getDoctrine()->getRepository(District::class);

        $entity = $repository->findAll();

        return $this->render('default/listDistricts.html.twig', array(
            'entity' => $entity
        ));
    }

    /**
     * @Route("/list/orderBy={orderBy}&orderDirection={orderDirection}", name="list_order")
     */
    public function order($orderBy = 'id', $orderDirection = 'ASC')
    {
        $repository = $this->getDoctrine()->getRepository(District::class);

        $entity = $repository->findBy(
            array(),
            array($orderBy => strtoupper($orderDirection))
        );

        return $this->render('default/listDistricts.html.twig', array(
            'entity' => $entity
        ));
    }

    /**
     * @Route("/list/filterBy={filterBy}&filterValue={filterValue}", name="list_filter")
     */
    public function filter($filterBy = 'city', $filterValue = 'Gdańsk')
    {
        $repository = $this->getDoctrine()->getRepository(District::class);

        $entity = $repository->findBy(
            array($filterBy => $filterValue),
            array($orderBy => strtoupper($orderDirection))
        );

        return $this->render('default/listDistricts.html.twig', array(
            'entity' => $entity
        ));
    }
}
